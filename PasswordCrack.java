import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordCrack {

	// encrypted passwords and usernames
	public static HashMap<String, String> enc_passw_to_users = new HashMap<String, String>();

	// salt_key and username 
	public static HashMap<String, String> saltedkeys_users = new HashMap<String, String>();

	//dictionary words
	public static HashMap<String, String> words_dict = new HashMap<String, String>();

	public static TreeMap<String, String> found_passwords = new TreeMap<String, String>();


	static String dictionary;
	static String passwords_to_crack;
	static int cracked_counter = 0;

	/**
	 * @param args
	 * @throws FileNotFoundException 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 */
	public static void main(String[] args) throws FileNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		//input file1
		dictionary =args[0];
		//input file2
		passwords_to_crack =args[1];

		populate_dictionary();

		populateKPMaps();

		simplePasswordCracking();
		mangledPasswordCracking();

	}

	public static void simplePasswordCracking(){
		String salt_word;
		for(String x: words_dict.keySet()){
			for(String salt: saltedkeys_users.keySet()){
				salt_word = jcrypt.crypt(salt, x);
				if(enc_passw_to_users.containsKey(salt_word) && !found_passwords.containsKey(enc_passw_to_users.get(salt_word)) ){
					System.out.println("("+(cracked_counter +1 )+ ")Yeah!! I found the password { " + x + " } for " + enc_passw_to_users.get(salt_word));
					found_passwords.put(enc_passw_to_users.get(salt_word), x);
					++cracked_counter;
				}
			}

		}

	}

	public static String prependCharacter(String ch, String word){
		return ch.concat(word);
	}

	public static String appendCharacter(String ch, String word){
		return word.concat(ch);
	}

	public static String deleteFirstCharacter(String word){
		return word.substring(1);
	}

	public static String deleteLastCharacter(String word){
		return word.substring(0, word.length()-1);
	}

	public static String reverseString(String word){
		String result = "";
		for(int i = word.length()-1 ; i >= 0 ; --i){
			result += word.charAt(i);
		}
		return result;
	}

	public static String duplicateString(String word){
		return word.concat(word);
	}

	public static String reflectFrontString(String word){
		return word.concat(reverseString(word));
	}
	public static String reflectBackString(String word){
		return reverseString(word).concat(word);
	}
	public static String capitalizeFirst(String word){

		return word.substring(0,1).toUpperCase().concat(word.substring(1));
	}
	public static String toUpperCaseString(String word){

		return word.toUpperCase();
	}


	public static String toLowerCaseString(String word){

		return word.toLowerCase();
	}
	public static String nCapitalize(String word){
		return word.substring(0,1).concat(word.substring(1).toUpperCase());
	}


	public static String toggleCase1(String word){
		String result = "";
		for(int i = 0 ; i+1 <= word.length(); i++){
			String a = word.substring(i,i+1);
			if(i%2 ==0){
				a=a.toUpperCase();
			}
			result += a;
		}
		return result;
	}
	public static String toggleCase2(String word){
		String result = "";
		for(int i = 0 ; i+1 <= word.length(); i++){
			String a = word.substring(i,i+1);
			if(i%2 ==1){
				a=a.toUpperCase();
			}
			result += a;
		}
		return result;
	}
	public static void mangledPasswordCracking() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		final int num_passwords_to_break = saltedkeys_users.size();

		int mangle_counter = 0;

		String [] methods ={"prependCharacter", "appendCharacter", "deleteFirstCharacter",
				"deleteLastCharacter", "reverseString", "duplicateString","reflectFrontString",
				"reflectBackString","toUpperCaseString","toLowerCaseString","capitalizeFirst", "nCapitalize", "toggleCase1", "toggleCase2"};

		Class[] cArg;
		while(cracked_counter < num_passwords_to_break){
			while(cracked_counter < num_passwords_to_break){
				Method m ;
				int method_pos = mangle_counter % 14;
				if(method_pos== 0){//First prepend a character to the string ...
					cArg = new Class[2];
					cArg[0] = String.class;
					cArg[1] = String.class;
					m = PasswordCrack.class.getMethod(methods[method_pos], cArg);
				}
				else if(method_pos % 14 == 1){
					cArg = new Class[2];
					cArg[0] = String.class;
					cArg[1] = String.class;
					m = PasswordCrack.class.getMethod(methods[method_pos], cArg);
				}
				else{
					cArg = new Class[1];
					cArg[0] = String.class;
					//			        cArg[1] = String.class;
					m = PasswordCrack.class.getMethod(methods[method_pos], cArg);
				}


				String salt_word;
				for(int c = 33; c <=126 ;++c){
					for(String x: words_dict.keySet()){

						if(method_pos == 0 || method_pos == 1){
							String a=""+(char)c;
							x = (String) m.invoke(String.class.getClass(),a ,x);
						}else{
							x = (String) m.invoke(String.class.getClass(),x);
						}
						for(String salt: saltedkeys_users.keySet()){


							salt_word = jcrypt.crypt(salt, x);
							if(enc_passw_to_users.containsKey(salt_word) && !found_passwords.containsKey(enc_passw_to_users.get(salt_word))){

								found_passwords.put(enc_passw_to_users.get(salt_word), x);
								System.out.println("("+ (cracked_counter +1) + ") Yeah!! I found the password { " + x + " } for " + enc_passw_to_users.get(salt_word));
								++cracked_counter;
							}
						}

					}

				}
				mangle_counter++;
			}
		}
	}

	public static void populate_dictionary() throws FileNotFoundException{
		File dictionary_file = new File(dictionary);
		Scanner sc_dict= new Scanner(dictionary_file);
		while(sc_dict.hasNextLine()){
			words_dict.put(sc_dict.nextLine(), "");
		}

		sc_dict.close();
	}


	public static void populateKPMaps() throws FileNotFoundException{
		File passwords_to_crack_file = new File(passwords_to_crack);
		Scanner sc_pass= new Scanner(passwords_to_crack_file);

		String regex = "(.*):(.*):(.*):(.*):(.*):(.*):(.*)";
		//1:username
		//2: encrypted password
		//5: Name and lastName
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher;

		String username ="";
		String salt= "";
		String eP ="";

		while(sc_pass.hasNextLine()){
			String Fname_Lname[] = new String[2];
			matcher= pattern.matcher(sc_pass.nextLine());
			matcher.find();

			username = matcher.group(1);
			salt = matcher.group(2).substring(0, 2);

			//Populates the salt and username map
			saltedkeys_users.put(salt, username);


			eP = matcher.group(2);	


			//Populates the hashed password and username
			enc_passw_to_users.put( eP, username);


			Fname_Lname = matcher.group(5).split("\\s");

			//Seeds first and last name to the word list
			words_dict.put(Fname_Lname[0], "");
			words_dict.put(Fname_Lname[1], "");

			words_dict.put(username, "");
		}

		sc_pass.close();
	}

}
