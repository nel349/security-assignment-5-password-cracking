FIRSTNAME : Norman;
LASTNAME : Lopez;
UTEID :  nel349;
CSACCOUNT : nel349;
EMAIL : noell.lpz@utexas.edu;

compile program:

	javac PasswordCrack.java

run program:

	java PasswordCrack inputFile1 inputFile2

I was able to crack the following passwords in less than 1 minute: (15/20)

(1)Yeah!! I found the password { amazing } for samantha
(2)Yeah!! I found the password { michael } for michael
(3)Yeah!! I found the password { Salizar } for maria
(4) Yeah!! I found the password { rdoctor } for morgan
(5) Yeah!! I found the password { abort6 } for benjamin
(6) Yeah!! I found the password { icious } for james
(7) Yeah!! I found the password { squadro } for alexander
(8) Yeah!! I found the password { eeffoc } for tyler
(9) Yeah!! I found the password { liagiba } for abigail
(10) Yeah!! I found the password { keyskeys } for nicole
(11) Yeah!! I found the password { doorrood } for jennifer
(12) Yeah!! I found the password { enoggone } for connor
(13) Yeah!! I found the password { THIRTY } for victor
(14) Yeah!! I found the password { Impact } for evan
(15) Yeah!! I found the password { sATCHEL } for jack